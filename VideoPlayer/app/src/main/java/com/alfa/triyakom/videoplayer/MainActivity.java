package com.alfa.triyakom.videoplayer;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class MainActivity extends ActionBarActivity {

    @InjectView(R.id.mvCustomVid)
    MyVideoView mvCustomVid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        mvCustomVid.setVideoPath("http://rmcdn.2mdn.net/MotifFiles/html/1248596/android_1330378998288.mp4");
    }

}