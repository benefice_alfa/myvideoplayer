package com.alfa.triyakom.videoplayer;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class ActivityViewFull extends ActionBarActivity {

    @InjectView(R.id.mvCustomVid)
    MyVideoView mvCustomVid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_full_screen);
        ButterKnife.inject(this);
        mvCustomVid.setVideoPath(getIntent().getExtras().getString(MyVideoView.VIDEO_PATH_KEY));
    }

}