package com.alfa.triyakom.videoplayer;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.VideoView;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by IT11 on 4/15/2015.
 */
public class MyVideoView extends FrameLayout implements View.OnClickListener {
    public static final String VIDEO_PATH_KEY = "video_path_key";
    private final int BTN_MINI_SHOW_PLAY = 0;
    private final int BTN_MINI_SHOW_PAUSE = 1;

    @InjectView(R.id.videoView)
    VideoView vidView;
    @InjectView(R.id.btnImgPlay)
    ImageButton btnImgPlay;
    @InjectView(R.id.btnMiniPlay)
    ImageButton btnMiniPlay;
    @InjectView(R.id.sbVideo)
    SeekBar sbVideo;
    @InjectView(R.id.btnFullScreen)
    ImageButton btnFullScreen;

    private String videoPath;// = "http://rmcdn.2mdn.net/MotifFiles/html/1248596/android_1330378998288.mp4";
    private Handler videoHandler = new Handler();
    private static ProgressDialog progressDialog;
    private boolean startTracking;
    private static int stopPosition;

    public MyVideoView(Context context) {
        super(context);
        init();
    }

    public MyVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View parent = LayoutInflater.from(getContext()).inflate(R.layout.video_view_custom, this, false);
        this.addView(parent);
        ButterKnife.inject(this,parent);
        btnImgPlay.setOnClickListener(this);
        vidView.setOnClickListener(this);
        btnMiniPlay.setOnClickListener(this);
        btnMiniPlay.setTag(BTN_MINI_SHOW_PLAY);
        btnFullScreen.setOnClickListener(this);
        startTracking = false;
        sbVideo.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                vidView.pause();
                startTracking = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                if (startTracking) {
                    stopPosition = seekBar.getProgress() * vidView.getDuration() / 100;
                    resumeVideo();
                }
                startTracking = false;
            }
        });

        stopPosition = 0;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    private void playVideo() {
        if (sbVideo.getProgress() == 0) {
            progressDialog = ProgressDialog.show(getContext(), "", "Buffering video...", true);
            progressDialog.setCancelable(true);
            try {
//                getContext().getWindow().setFormat(PixelFormat.TRANSLUCENT);
                Uri video = Uri.parse(videoPath);
                vidView.setVideoURI(video);
                vidView.requestFocus();
                vidView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    public void onPrepared(MediaPlayer mp) {
                        progressDialog.dismiss();
                        btnImgPlay.setVisibility(View.INVISIBLE);
                        vidView.start();
                        resetProgressBar();
                        startProgressBar();
                        mp.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                            @Override
                            public void onSeekComplete(MediaPlayer mp) {
                                vidView.start();
                            }
                        });
                    }

                });
                vidView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        toggleBtnMiniPlay();
                        sbVideo.setProgress(0);
                        btnImgPlay.setVisibility(View.VISIBLE);
                    }
                });
                vidView.setOnClickListener(this);
            } catch (Exception e) {
                progressDialog.dismiss();
            }
        } else {
            resumeVideo();
        }
        toggleBtnMiniPlay();
    }

    private void toggleBtnMiniPlay() {
        int tagBtnMini = Integer.parseInt(btnMiniPlay.getTag() + "");
        switch (tagBtnMini) {
            case BTN_MINI_SHOW_PLAY:
                btnMiniPlay.setTag(BTN_MINI_SHOW_PAUSE);
                btnMiniPlay.setImageResource(R.mipmap.small_pause);
                break;
            case BTN_MINI_SHOW_PAUSE:
                btnMiniPlay.setTag(BTN_MINI_SHOW_PLAY);
                btnMiniPlay.setImageResource(R.mipmap.small_play);
                break;
        }
    }

    private void startProgressBar() {
        videoHandler.postDelayed(updateTimeTask, 100);
    }

    private Runnable updateTimeTask = new Runnable() {
        @Override
        public void run() {
            long totalDuration = vidView.getDuration();
            int currentDuration = vidView.getCurrentPosition();

            // update progress bar
            int progress = getProgressPercentage(currentDuration, totalDuration);
            sbVideo.setProgress(progress);

            sbVideo.setSecondaryProgress(vidView.getBufferPercentage());
            if (vidView.isPlaying()) {
                videoHandler.postDelayed(this, 200);
            }
        }
    };

    private void resetProgressBar() {
        sbVideo.setProgress(0);
        sbVideo.setSecondaryProgress(0);
    }

    public int getProgressPercentage(long currentDuration, long totalDuration) {
        Double percentage = (double) 0;

        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);
        // calculating percentage
        percentage = (((double) currentSeconds) / totalSeconds) * 100;
        // return percentage
        return percentage.intValue();
    }

//    @Override
//    protected void onPause() {
//        super.onPause();
//        pauseVideo();
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnImgPlay:
                playVideo();
                break;
            case R.id.videoView:
                pauseVideo();
                break;
            case R.id.btnFullScreen:
                pauseVideo();
                Intent intent = new Intent(getContext(),ActivityViewFull.class);
                intent.putExtra(VIDEO_PATH_KEY,videoPath);
                getContext().startActivity(intent);
                break;
            case R.id.btnMiniPlay:
                int tagBtnMini = (int) btnMiniPlay.getTag();
                switch (tagBtnMini) {
                    case BTN_MINI_SHOW_PAUSE:
                        pauseVideo();
                        break;
                    case BTN_MINI_SHOW_PLAY:
                        playVideo();
                        break;
                }
                break;
        }
    }

    public void pauseVideo() {

        stopPosition = vidView.getCurrentPosition();
        vidView.pause();
        btnImgPlay.setVisibility(View.VISIBLE);
        toggleBtnMiniPlay();
    }

    public void resumeVideo() {
        btnImgPlay.setVisibility(View.INVISIBLE);
        vidView.seekTo(stopPosition);
//        vidView.start();
        startProgressBar();
    }
}
